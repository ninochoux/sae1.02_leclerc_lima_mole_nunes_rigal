package body TAD_Coordonnee is
   ---------------------------
   -- construireCoordonnees --
   ---------------------------
   function construireCoordonnees
     (ligne : Integer; colonne : Integer) return Type_Coordonnee
   is
      c:Type_Coordonnee;
   begin
      c.ligne:=ligne;
      c.colonne:=colonne;
      return c;
   end construireCoordonnees;
   ------------------
   -- obtenirLigne --
   ------------------
   function obtenirLigne (c : Type_Coordonnee) return Integer is
   begin
      return c.ligne;
   end obtenirLigne;
   --------------------
   -- obtenirColonne --
   --------------------
   function obtenirColonne (c : Type_Coordonnee) return Integer is
   begin
      return c.colonne;
   end obtenirColonne;
   ------------------
   -- obtenirCarre --
   ------------------
   function obtenirCarre (c : Type_Coordonnee) return Integer is
      carre:Integer;
   begin
      if obtenirLigne(c) >= 7 then
         if obtenirColonne(c) >=7 then
            carre:=9;
         elsif obtenirColonne(c) >=4 then
            carre:=8;
         elsif obtenirColonne(c) >=1 then
            carre:=7;
         end if;
      elsif obtenirLigne(c) >= 4 then
         if obtenirColonne(c) >=7 then
            carre:=6;
         elsif obtenirColonne(c) >=4 then
            carre:=5;
         elsif c.colonne >=1 then
            carre:=4;
         end if;
      elsif obtenirLigne(c) >= 1 then
         if obtenirColonne(c) >=7 then
            carre:=3;
         elsif obtenirColonne(c) >=4 then
            carre:=2;
         elsif obtenirColonne(c) >=1 then
            carre:=1;
         end if;
      end if;
      return carre;
   end obtenirCarre;
   ----------------------------
   -- obtenirCoordonneeCarre --
   ----------------------------
   function obtenirCoordonneeCarre (numCarre : Integer) return Type_Coordonnee
   is
      c:Type_Coordonnee;
   begin
      case numCarre is
         when 1 => c := construireCoordonnees(1,1);
         when 2 => c := construireCoordonnees(1,4);
         when 3 => c := construireCoordonnees(1,7);
         when 4 => c := construireCoordonnees(4,1);
         when 5 => c := construireCoordonnees(4,4);
         when 6 => c := construireCoordonnees(4,7);
         when 7 => c := construireCoordonnees(7,1);
         when 8 => c := construireCoordonnees(7,4);
         when others => c:=construireCoordonnees(7,7);
         --j'ai mis le cas 9 en others comme dans la consigne
      end case;
      return c;
   end obtenirCoordonneeCarre;
end TAD_Coordonnee;
