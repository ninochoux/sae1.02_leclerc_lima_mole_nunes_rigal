pragma Ada_2012;
package body TAD_grilleSudoku is
   ----------------------
   -- construireGrille --
   ----------------------
   function construireGrille return Type_Grille is
      g : Type_Grille;
   begin
      for ligne in 1..9 loop
         for colonne in 1..9 loop
            g(ligne, colonne) := 0;
         end loop;
      end loop;
      return g;
   end construireGrille;
   --------------
   -- caseVide --
   --------------
   function caseVide
     (g : in Type_Grille; c : in Type_Coordonnee) return Boolean
   is
   begin
      if g(obtenirLigne(c), obtenirColonne(c)) = 0 then
         return true;
      else
         return false;
      end if;
   end caseVide;
   --------------------
   -- obtenirChiffre --
   --------------------
   function obtenirChiffre
     (g : in Type_Grille; c : in Type_Coordonnee) return Integer
   is
   begin
      if caseVide(g, c) then
         raise OBTENIR_CHIFFRE_NUL;
      end if;
      return g(obtenirLigne(c), obtenirColonne(c));
   end obtenirChiffre;
   --------------------
   -- nombreChiffres --
   --------------------
   function nombreChiffres
     (g : in Type_Grille) return Integer
   is
      compteur      : integer;
      c       : Type_Coordonnee;
   begin
      compteur := 0;
      for ligne in 1..9 loop
         for colonne in 1..9 loop
            c := construireCoordonnees(ligne, colonne);
            if not caseVide(g, c) then
               compteur := compteur + 1;
            end if;
         end loop;
      end loop;
      return compteur;
   end nombreChiffres;
   ------------------
   -- fixerChiffre --
   ------------------
   procedure fixerChiffre
     (g : in out Type_Grille; c : in Type_Coordonnee; v : in Integer; cpt : in out Integer)
   is
   begin
      if not caseVide(g, c) then
         raise FIXER_CHIFFRE_NON_NUL;
      end if;
      g(obtenirLigne(c), obtenirColonne(c)) := v;
      cpt := cpt + 1;
   end fixerChiffre;
   ---------------
   -- viderCase --
   ---------------
   procedure viderCase (g : in out Type_Grille; c : in out Type_Coordonnee) is
   begin
      if g(obtenirLigne(c), obtenirColonne(c)) = 0 then
         raise VIDER_CASE_VIDE;
      end if;
      g(obtenirLigne(c), obtenirColonne(c)) := 0;
   end viderCase;
   ----------------
   -- estRemplie --
   ----------------
   function estRemplie (g : in Type_Grille) return Boolean is
      compteur           : integer;
      c            : Type_Coordonnee;
      TailleGrille : integer;
   begin
      compteur := 0;
      TailleGrille := 81;
      for ligne in 1..9 loop
         for colonne in 1..9 loop
            c := construireCoordonnees(ligne, colonne);
            if caseVide(g, c) = false then
               compteur := compteur + 1;
            end if;
         end loop;
      end loop;
      if compteur = TailleGrille then
         return true;
      else
         return false;
      end if;
   end estRemplie;
   ------------------------------
   -- obtenirChiffresDUneLigne --
   ------------------------------
   function obtenirChiffresDUneLigne
     (g : in Type_Grille; numLigne : in Integer) return Type_Ensemble
   is
      ens : Type_Ensemble;
   begin
      ens := construireEnsemble;
      for chiffreLigne in 1..9 loop
         if not caseVide(g, construireCoordonnees(numLigne, chiffreLigne)) then
            ajouterChiffre(ens, g(numLigne, chiffreLigne));
         end if;
      end loop;
      return ens;
   end obtenirChiffresDUneLigne;
   --------------------------------
   -- obtenirChiffresDUneColonne --
   --------------------------------
   function obtenirChiffresDUneColonne
     (g : in Type_Grille; numColonne : in Integer) return Type_Ensemble
   is
      ens : Type_Ensemble;
   begin
      ens := construireEnsemble;
      for chiffreColonne in 1..9 loop
         if not caseVide(g, construireCoordonnees(chiffreColonne, numColonne)) then
            ajouterChiffre(ens, g(chiffreColonne, numColonne));
         end if;
      end loop;
      return ens;
   end obtenirChiffresDUneColonne;
   -----------------------------
   -- obtenirChiffresDUnCarre --
   -----------------------------
   function obtenirChiffresDUnCarre
     (g : in Type_Grille; numCarre : in Integer) return Type_Ensemble
   is
      ens : Type_Ensemble;
      coor : Type_Coordonnee;
      coor_actu : Type_Coordonnee;
   begin
      ens := construireEnsemble;
      coor := obtenirCoordonneeCarre(numCarre);
      for ligne in obtenirLigne(coor)..obtenirLigne(coor)+2 loop
         for colonne in obtenirColonne(coor)..obtenirColonne(coor)+2 loop
            coor_actu := construireCoordonnees(ligne, colonne);
            if not caseVide(g,coor_actu) then
               ajouterChiffre(ens,obtenirChiffre(g,coor_actu));
            end if;
         end loop;
      end loop;
      return ens;
   end obtenirChiffresDUnCarre;
   ------------------
   -- copierGrille --
   ------------------
   procedure copierGrille(gPrecedente: in Type_Grille; gNouvelle: in out Type_Grille) is
   begin
      for ligne in 1..9 loop
         for colonne in 1..9 loop
            gNouvelle(ligne, colonne) := gPrecedente(ligne,colonne);
         end loop;
      end loop;
   end copierGrille;
end TAD_grilleSudoku;
