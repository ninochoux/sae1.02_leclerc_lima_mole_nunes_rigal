pragma Ada_2012;
package body TAD_ensemble is
   ------------------------
   -- construireEnsemble --
   ------------------------
   function construireEnsemble return Type_Ensemble is
      Ens : Type_Ensemble;
   begin
      for chiffre in 1..9 loop
         Ens(chiffre) := False;
      end loop;
      return Ens;
   end construireEnsemble;
   ------------------
   -- ensembleVide --
   ------------------
   function ensembleVide (e : in Type_Ensemble) return Boolean is
      resultats : Integer;
   begin
      resultats := 0;
      for chiffre in 1..9 loop
         if e(chiffre) then
            resultats := resultats + 1;
         end if;
      end loop;
      if resultats = 0 then
         return True;
      else
         return False;
      end if;
   end ensembleVide;
   -----------------------
   -- appartientChiffre --
   -----------------------
   function appartientChiffre
     (e : in Type_Ensemble; v : Integer) return Boolean
   is
   begin
      return e(v);
   end appartientChiffre;
   --------------------
   -- nombreChiffres --
   --------------------
   function nombreChiffres (e : in Type_Ensemble) return Integer is
      resultats : Integer;
   begin
      resultats := 0;
      for chiffre in 1..9 loop
         if e(chiffre) = True then
            resultats := resultats + 1;
         end if;
      end loop;
      return resultats;
   end nombreChiffres;
   --------------------
   -- ajouterChiffre --
   --------------------
   procedure ajouterChiffre (e : in out Type_Ensemble; v : in Integer) is
   begin
      if e(v) then
         raise APPARTIENT_ENSEMBLE;
      end if;
      e(v) := True;
   end ajouterChiffre;
   --------------------
   -- retirerChiffre --
   --------------------
   procedure retirerChiffre (e : in out Type_Ensemble; v : in Integer) is
   begin
      if not e(v) then
         raise NON_APPARTIENT_ENSEMBLE;
      end if;
      e(v) := False;
   end retirerChiffre;
end TAD_ensemble;
