pragma Ada_2012;
package body resolutions is
   -----------------------
   -- estChiffreValable --
   -----------------------
   function estChiffreValable
     (g : in Type_Grille; v : Integer; c : Type_Coordonnee) return Boolean
   is
   begin
      if caseVide (g,c) /= True then
         raise CASE_NON_VIDE;
      end if;
      if appartientChiffre (obtenirChiffresDUnCarre (g, obtenirCarre (c)), v)
      then
         return False;
      elsif appartientChiffre
        (obtenirChiffresDUneColonne (g, obtenirColonne (c)), v)
      then
         return False;
      elsif appartientChiffre
        (obtenirChiffresDUneLigne (g, obtenirLigne (c)), v)
      then
         return False;
      else
         return True;
      end if;
   end estChiffreValable;
   -------------------------------
   -- obtenirSolutionsPossibles --
   -------------------------------
   function obtenirSolutionsPossibles
     (g : in Type_Grille; c : in Type_Coordonnee) return Type_Ensemble
   is
      ensemble : Type_Ensemble;
   begin
      if caseVide (g, c) /= True then
         raise CASE_NON_VIDE;
      end if;
      ensemble := construireEnsemble;
      for Possible in 1 .. 9 loop
         if estChiffreValable (g, Possible, c) then
            ajouterChiffre (ensemble, Possible);
         end if;
      end loop;
      return ensemble;
   end obtenirSolutionsPossibles;
   ------------------------------------------
   -- rechercherSolutionUniqueDansEnsemble --
   ------------------------------------------
   function rechercherSolutionUniqueDansEnsemble
     (resultats : in Type_Ensemble) return Integer
   is
      res : Integer;
   begin
      if ensembleVide (resultats) then
         raise ENSEMBLE_VIDE;
      end if;
      if nombreChiffres (resultats) <= 1 then
         raise PLUS_DE_UN_CHIFFRE;
      end if;
      for Possible in 1..9 loop
         if appartientChiffre (resultats, Possible) then
            res := Possible;
         end if;
      end loop;
      return res;
   end rechercherSolutionUniqueDansEnsemble;
   --------------------------
   -- estPossibiliteUnique --
   --------------------------
   function estPossibiliteUnique
     (g : in Type_Grille; Possibilite : in Integer; enr : in enr_Coordonnees)
      return Boolean
   is
   begin
      if enr.taille = 0 then
         return False;
      end if;
      for Possible in 1 .. enr.taille loop
         if estChiffreValable (g, Possibilite, enr.tab (Possible)) then
            return False;
         end if;
      end loop;
      return True;
   end estPossibiliteUnique;
   --------------------------------------
   -- obtenirAutreCoordonneesVideLigne --
   --------------------------------------
   function obtenirAutreCoordonneesVideLigne
     (g : in Type_Grille; c : in Type_Coordonnee) return enr_Coordonnees
   is
      ligne      : Integer;
      colonne    : Integer;
      enr        : enr_Coordonnees;
      coordonnee : Type_Coordonnee;
   begin
      enr.taille := 0;
      ligne      := obtenirLigne (c);
      colonne    := obtenirColonne (c);
      for col in 1 .. 9 loop

         coordonnee := construireCoordonnees (ligne, col);
         if caseVide (g, coordonnee) and col /= colonne then
            enr.taille           := enr.taille + 1;
            enr.tab (enr.taille) := coordonnee;
         end if;
      end loop;
      return enr;
   end obtenirAutreCoordonneesVideLigne;
   ----------------------------------------
   -- obtenirAutreCoordonneesVideColonne --
   ----------------------------------------
   function obtenirAutreCoordonneesVideColonne
     (g : in Type_Grille; c : in Type_Coordonnee) return enr_Coordonnees
   is
      ligne      : Integer;
      colonne    : Integer;
      enr        : enr_Coordonnees;
      coordonnee : Type_Coordonnee;
   begin
      enr.taille := 0;
      ligne      := obtenirLigne (c);
      colonne    := obtenirColonne (c);
      for li in 1 .. 9 loop
         coordonnee := construireCoordonnees (li, colonne);
         if caseVide (g, coordonnee) and li /= ligne then
            enr.taille           := enr.taille + 1;
            enr.tab (enr.taille) := coordonnee;
         end if;
      end loop;
      return enr;
   end obtenirAutreCoordonneesVideColonne;
   --------------------------------------
   -- obtenirAutreCoordonneesVideCarre --
   --------------------------------------
   function obtenirAutreCoordonneesVideCarre
     (g : in Type_Grille; c : in Type_Coordonnee) return enr_Coordonnees
   is
      enr            : enr_Coordonnees;
      ligneOrigine   : Integer;
      ligne          : Integer;
      colonneOrigine : Integer;
      colonne        : Integer;
      coordonnee     : Type_Coordonnee;
      coor           : Type_Coordonnee;
   begin
      enr.taille     := 0;
      colonneOrigine := obtenirColonne (c);
      ligneOrigine   := obtenirLigne (c);
      coordonnee     := obtenirCoordonneeCarre (obtenirCarre (c));
      ligne          := obtenirLigne (coordonnee);
      colonne        := obtenirColonne (coordonnee);
      for li in ligne .. ligne + 2 loop
         for col in colonne .. colonne + 2 loop
            coor := construireCoordonnees (li, col);
            if caseVide (g, coor) and
              not (li = ligneOrigine and col = colonneOrigine)
            then
               enr.taille           := enr.taille + 1;
               enr.tab (enr.taille) := coor;
            end if;
         end loop;
      end loop;
      return enr;
   end obtenirAutreCoordonneesVideCarre;
   --------------------
   -- resoudreSudoku --
   --------------------
   procedure resoudreSudoku
     (g : in out Type_Grille; trouve : out Boolean; cpt : in out Integer)
   is
      coordonnee      : Type_Coordonnee;
      possibiliteCase : Type_Ensemble;
      fixe            : Boolean;
      ng              : Type_Grille;
      nbPossibilite   : Integer;
   begin
      fixe := True;
      while not estRemplie (g) and fixe loop
         fixe := False;
         for ligne in 1 .. 9 loop
            for colonne in 1 .. 9 loop
               coordonnee := construireCoordonnees (ligne, colonne);
               if caseVide (g, coordonnee) then
                  possibiliteCase := obtenirSolutionsPossibles (g, coordonnee);
                  if not ensembleVide (possibiliteCase) then
                     if nombreChiffres (possibiliteCase) = 1 then
                        for possibilite in 1 .. 9 loop
                           if appartientChiffre (possibiliteCase, possibilite) then
                              fixerChiffre (g, coordonnee, possibilite, cpt);
                              fixe := True;
                           end if;
                        end loop;
                     else
                        for possibilite in 1 .. 9 loop
                           if appartientChiffre (possibiliteCase, possibilite) then
                              if estPossibiliteUnique
                                (g, possibilite,
                                 obtenirAutreCoordonneesVideLigne
                                   (g, coordonnee))
                              then
                                 fixerChiffre (g, coordonnee, possibilite, cpt);
                                 fixe := True;
                                 exit;
                              elsif estPossibiliteUnique
                                (g, possibilite,
                                 obtenirAutreCoordonneesVideColonne
                                   (g, coordonnee))
                              then
                                 fixerChiffre (g, coordonnee, possibilite, cpt);
                                 fixe := True;
                                 exit;
                              elsif estPossibiliteUnique
                                (g, possibilite,
                                 obtenirAutreCoordonneesVideCarre
                                   (g, coordonnee))
                              then
                                 fixerChiffre (g, coordonnee, possibilite, cpt);
                                 fixe := True;
                                 exit;
                              end if;
                           end if;
                        end loop;
                     end if;
                  end if;
               end if;
            end loop;
         end loop;
      end loop;
      trouve := estRemplie (g);
      if not trouve then
         for ligne in 1 .. 9 loop
            for colonne in 1 .. 9 loop
               coordonnee := construireCoordonnees (ligne, colonne);
               if caseVide (g, coordonnee) then
                  possibiliteCase := obtenirSolutionsPossibles (g, coordonnee);
                  nbPossibilite   := 0;
                  for ChiffrePossible in 1 .. 9 loop
                     if appartientChiffre (possibiliteCase, ChiffrePossible)
                     then
                        nbPossibilite := nbPossibilite + 1;
                     end if;
                  end loop;
                  if nbPossibilite /= 0 then
                     for ChiffrePossible in 1 .. 9 loop
                        if appartientChiffre (possibiliteCase, ChiffrePossible)
                        then
                           copierGrille (g, ng);
                           fixerChiffre (ng, coordonnee, ChiffrePossible, cpt);
                           resoudreSudoku (ng, trouve, cpt);
                           if trouve then
                              copierGrille (ng, g);
                              return;
                           end if;
                        end if;
                     end loop;
                  else
                     return;
                  end if;
               end if;
            end loop;
         end loop;
      end if;
   end resoudreSudoku;
end resolutions;
